import logging
import math

import cv2
import imageio
import numpy as np
import pandas as pd
from PIL import Image

from fw_gear_oct_alignment.normxcorr2 import normxcorr2

log = logging.getLogger(__name__)


# package normxcorr2python needs to avoid "-" symbols (can't have slashes), so they are substituted by '_' symbol.


def batch_aligning(
    imArr,
    smooth_level=0,
    centering_criteria_percentage=5,
    xOffset_limit=15,
    rotate_first_slice=False,
):
    """
    Aligns all images in an array.
    Args:
        imArr: numpy array. Numpy array of misaligned OCT slices. Shape: Number of slices x Height x Width.
        smooth_level: int. Kernel size for median blurring algorithm on reference and moving image.
        centering_criteria_percentage: int. Percentage. The first slice will be centered until the retina is +- this
        centering criteria * image height.
        xOffset_limit: int. X offset shift limitation on alignment to avoid carryover misalignment
    Returns:
        imArr_aligned: numpy array. Aligned OCT images. Shape: Number of slices x Height x Width.
        output: pandas dataframe. Dataframe with alignment metadata to be exported as csv.
    """

    num_slices = imArr.shape[0]
    print("Number of slices:", num_slices)

    # initialized aligned array
    imArr_aligned = np.zeros([num_slices, imArr.shape[1], imArr.shape[2]])

    # preprocess first slice: center and rotate.
    log.info("Preprocessing first slice: centering retina and rotating...")
    centered_slice = center_image(imArr[0], centering_criteria_percentage)
    if rotate_first_slice:
        rotated_slice = rotate_image(centered_slice)
    else:
        rotated_slice = centered_slice.copy()  # rotate_image(centered_slice)

    log.info("Done preprocessing first OCT slice.")

    # copy first image since it won't be aligned
    imArr_aligned[0] = rotated_slice

    # the second argument is the one that gets aligned

    # initialize output values
    xoffSets = [0]
    yoffSets = [0]
    maxCorrs = [1]
    log.info("\tStarting batch aligning...")
    if xOffset_limit > 0:
        log.info(
            "\tWarning: Note that the X offset shift is limited to '%s' pixels to avoid carryover misalignment",
            xOffset_limit,
        )

    for image in range(num_slices - 1):
        imAligned, xoffSet, yoffSet, maxCorr = align_oct(
            imArr_aligned[image], imArr[image + 1], smooth_level
        )
        xoffSets.append(xoffSet)
        yoffSets.append(yoffSet)
        maxCorrs.append(round(maxCorr, 3))
        imArr_aligned[image + 1] = imAligned
    log.info("\tFinished batch aligning")

    # generate csv with metadata
    output = pd.DataFrame()
    output["Slice Number"] = range(num_slices)
    output["X-Offset"] = xoffSets
    output["Y-Offset"] = yoffSets
    output["Max Correlation"] = maxCorrs

    return imArr_aligned, output


def align_oct(ref_image, mov_image, smooth_level=0, xOffset_limit=15):
    """
    Main function aligning OCT slices.
    Args:
        ref_image: numpy array. Reference image for alignment.
        mov_image: numpy array. Image to be aligned.
        smooth_level: int. Kernel size for median blurring algorithm on reference and moving image.
        xOffset_limit: int. X offset shift limitation on alignment to avoid carryover misalignment.
    Returns:
        img_translation: numpy array. Moving image aligned to reference image.
    """
    # apply smoothing to images if optional argument smooth_level specified
    mov_image_original = mov_image.copy()
    if smooth_level > 0:
        ref_image = cv2.medianBlur(ref_image, smooth_level)
        mov_image = cv2.medianBlur(mov_image, smooth_level)
    # calculate normalized crosscorrelation between reference image and unaligned image.
    crossCorrMap = normxcorr2(ref_image, mov_image)
    # find X,Y coordinates of peak in normalized crosscorrelation map.
    ypeak, xpeak = np.unravel_index(crossCorrMap.argmax(), crossCorrMap.shape, "C")
    # maximum correlation between two images at one particular point
    maxCorr = crossCorrMap[ypeak, xpeak]
    # y and x offsets to align moving images to reference image using X and y peaks and adjusting for image size and padding.
    yoffSet = ypeak - ref_image.shape[0] + 1
    xoffSet = xpeak - ref_image.shape[1] + 1
    if xoffSet > 0:
        xoffSet = min(xOffset_limit, xoffSet)
    elif xoffSet < 0:
        xoffSet = max(-xOffset_limit, xoffSet)
    num_rows, num_cols = ref_image.shape
    # create translation matrix for image alignment
    translation_matrix = np.float32([[1, 0, -xoffSet], [0, 1, -yoffSet]])
    # transform moving image based on translation matrix
    img_translation = cv2.warpAffine(
        mov_image_original, translation_matrix, (num_cols, num_rows)
    )

    return img_translation, xoffSet, yoffSet, maxCorr


def generate_video(imArray_aligned, outputpath):
    """
    Generates video from OCT aligned slices.
    Args:
        imArray_aligned: numpy array.
        outputpath: str. Path to write out video with OCT aligned images.
    """

    # transform numpy array to imageio array.
    imArray_aligned = imageio.core.util.Array(imArray_aligned)
    num_slices = imArray_aligned.shape[0]

    # initialize video
    video_writer = imageio.get_writer(outputpath, macro_block_size=None)
    # append all slices to video writer
    for image in range(num_slices):
        video_writer.append_data(imArray_aligned[image])

    # write out video and close down
    video_writer.close()


def center_image(image, centering_criteria_percentage=5):
    """
    Centers image and pads it, so that retina is in the middle.
    Args:
        -image: numpy array. Image to be centered.
        -centering_criteria_percentage: int. Percentage. The image to be centered until the retina is +- this centering criteria * image height.
    Returns:
        -image_shifted: numpy array. centered image.
    """
    centering_criteria_pixel = (centering_criteria_percentage / 100) * image.shape[0]
    image_shifted = image.copy()
    across_columns = np.sum(image_shifted, axis=1)
    max_bright_spot = np.argmax(across_columns)
    while (
        max_bright_spot > ((image_shifted.shape[0] / 2) + centering_criteria_pixel)
    ) or (max_bright_spot < ((image_shifted.shape[0] / 2) - centering_criteria_pixel)):
        if max_bright_spot > ((image_shifted.shape[0] / 2) + centering_criteria_pixel):
            image_shifted = shift_image(image_shifted, 0, -1)
            max_bright_spot -= 1
        elif max_bright_spot < (
            (image_shifted.shape[0] / 2) - centering_criteria_pixel
        ):
            image_shifted = shift_image(image_shifted, 0, +1)
            max_bright_spot += 1
    return image_shifted


def shift_image(X, dx, dy):
    """
    Moves image and pads it.
    Args:
        -X: numpy array. Image to be shifted.
        -dx: int or float. X offset.
        -dy: int or float. Y offset.
    Returns:
        -X: numpy array. Shifted image.
    """
    X = np.roll(X, dy, axis=0)
    X = np.roll(X, dx, axis=1)
    if dy > 0:
        X[:dy, :] = 0
    elif dy < 0:
        X[dy:, :] = 0
    if dx > 0:
        X[:, :dx] = 0
    elif dx < 0:
        X[:, dx:] = 0
    return X


def rotate_image(image, maxLineGap=5):
    """
    Finds angle of rotation of an image and rotates.
    Args:
        -image: numpy array. Image to be rotated.
        -maxLineGap: integer. Maximum allowed gap between line segments to treat them as single line.
    Returns:
        -rotated image: numpy array. Rotated image.
    """
    img_before = image.copy()

    img_gray = np.uint8(img_before)
    img_edges = cv2.Canny(img_gray, 99, 99, apertureSize=3)
    lines = cv2.HoughLinesP(
        img_edges, 1, math.pi / 180.0, 100, minLineLength=100, maxLineGap=maxLineGap
    )

    angles = []

    for [[x1, y1, x2, y2]] in lines:
        cv2.line(img_before, (x1, y1), (x2, y2), (255, 0, 0), 3)
        angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
        angles.append(angle)

    median_angle = np.median(angles)

    rotated_image = Image.fromarray(image)
    rotated_image = np.array(rotated_image.rotate(median_angle))

    return rotated_image
