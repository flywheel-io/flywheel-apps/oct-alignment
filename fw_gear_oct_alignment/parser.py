"""Parser module to parse gear config.json."""
import logging
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext

from .aligner import OCTaligner

log = logging.getLogger(__name__)


# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[OCTaligner, dict]:
    """Parses gear_context config.json file and returns relevant inputs and
    options."""

    log.info("Parsing inputs from gear context...")

    # Get the config dictionary
    config = gear_context.config_json
    config_dict = config.get("config")

    # check that arguments are correct
    if ((config_dict.get("smooth_level") == 0)) | (
        (config_dict.get("smooth_level") > 0)
        & (config_dict.get("smooth_level") % 2 == 1)
    ):
        log.info("smooth_level: %s", config_dict.get("smooth_level"))
    else:
        raise ValueError("Smooth level must be above zero and an odd number.")

    if config_dict.get("centering_criteria_percentage") >= 0:
        log.info(
            "centering_criteria_percentage: %s",
            config_dict.get("centering_criteria_percentage"),
        )
    else:
        raise ValueError("Centering criteria percentage must be above zero.")

    if config_dict.get("xOffset_limit") >= 0:
        log.info("xOffset_limit: %s", config_dict.get("xOffset_limit"))
    else:
        raise ValueError("X offset limit must be above zero.")

    log.info("Rotation of first slice?: %s", config_dict.get("rotate_first_slice"))

    # Get the path to the raw input file
    path_raw_input = gear_context.get_input_path("raw_input")

    # Use an interface for the different file types
    ophtha_aligner = OCTaligner.factory(
        path_file=path_raw_input,
        output_dir=gear_context.output_dir,
        work_dir=gear_context.work_dir,
    )

    return ophtha_aligner, config_dict
