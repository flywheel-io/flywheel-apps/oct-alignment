import logging
import os
import re
import shutil
import sys
import time
import zipfile

import numpy as np
import pydicom
import SimpleITK as sitk
from fw_file.dicom import DICOM
from pydicom.filebase import DicomBytesIO

log = logging.getLogger(__name__)


def read_oct_numpy(filepath):
    """
    Args:
        filepath: str. Filepath to numpy array with OCT images.
    Returns:
        file: numpy array.
    """
    file = np.load(filepath)
    return file


def read_oct_dicom(filepath):

    dcm = DICOM(filepath)
    rows = dcm.rows
    columns = dcm.columns
    slices = dcm.Number_of_Frames
    deserialized_bytes = np.frombuffer(dcm.PixelData, dtype=np.uint8)
    arr = np.reshape(deserialized_bytes, newshape=(slices, rows, columns))
    return dcm, arr


def read_oct_dicom_zip(pathfile):
    """
    Unzips and reads dicom files in .dcm.zip format.
    Args:
        - pathfile. str: path to .dcm.zip file.
    Returns:
        - result: numpy array. Contains OCT slices.
    """
    zip_archive = zipfile.ZipFile(pathfile, "r")
    unzipped_file = unzip(zip_archive)
    log.warning(
        "Ordering slices by number in name. Please make sure that the zipped files are numbered in order."
    )
    pattern = r"(\d\d*)\..*"
    unzipped_file = sorted(
        unzipped_file, key=lambda x: int(re.findall(pattern, x[0])[0])
    )
    height, width = pydicom.dcmread(unzipped_file[0][1], force=True).pixel_array.shape
    result = np.zeros([len(unzipped_file), height, width])
    log.info("Reading unzipped DICOM files...")
    for idx in range(len(unzipped_file)):
        zip_archive = zipfile.ZipFile(pathfile, "r")
        unzipped_file = unzip(zip_archive)
        unzipped_file = sorted(
            unzipped_file, key=lambda x: int(re.findall(pattern, x[0])[0])
        )
        dcm = pydicom.dcmread(unzipped_file[idx][1], force=True)
        result[idx] = dcm.pixel_array
    log.info("Reading complete!")
    return result


def unzip(zip_archive):
    """
    Args:
        -zip_archive is a zipfile object (from zip_archive = zipfile.ZipFile(filename, 'r') for example)
    Returns:
        -file_list: a dictionary of file names and file like objects (StringIO's)
    The filter in the if statement skips directories and dot files
    """

    file_list = []
    for file_name in zip_archive.namelist():
        if not os.path.basename(file_name).startswith(".") and not file_name.endswith(
            "/"
        ):
            file_object = zip_archive.open(file_name, "r")
            file_like_object = DicomBytesIO(file_object.read())
            file_object.close()
            file_like_object.seek(0)
            name = os.path.basename(file_name)
            file_list.append((name, file_like_object))
    return file_list


def writeSlices(writer, series_tag_values, new_img, out_dir, i):
    """
    Write slices from numpy to DICOM as individual files.

    """
    image_slice = new_img[:, :, i]

    # Tags shared by the series.
    list(
        map(
            lambda tag_value: image_slice.SetMetaData(tag_value[0], tag_value[1]),
            series_tag_values,
        )
    )

    # Slice specific tags.
    #   Instance Creation Date
    image_slice.SetMetaData("0008|0012", time.strftime("%Y%m%d"))
    #   Instance Creation Time
    image_slice.SetMetaData("0008|0013", time.strftime("%H%M%S"))

    # Setting the type to CT so that the slice location is preserved and
    # the thickness is carried over.
    image_slice.SetMetaData("0008|0060", "CT")

    # (0020, 0032) image position patient determines the 3D spacing between
    # slices.
    #   Image Position (Patient)
    image_slice.SetMetaData(
        "0020|0032",
        "\\".join(map(str, new_img.TransformIndexToPhysicalPoint((0, 0, i)))),
    )
    #   Instance Number
    image_slice.SetMetaData("0020,0013", str(i))

    # Write to the output directory and add the extension dcm, to force
    # writing in DICOM format.
    writer.SetFileName(os.path.join(out_dir, str(i) + ".dcm"))
    writer.Execute(image_slice)


def process_zipped_dicom(filepath_out_dcm, imArr, output_dir_dcm):
    """
    Write slices from numpy to DICOM as individual files and zips them as .dcm.zip.
    Uses Simple ITK: https://simpleitk.readthedocs.io/en/v1.1.0/Examples/DicomSeriesReadModifyWrite/Documentation.html

    """
    new_arr = np.uint8(imArr)
    new_img = sitk.GetImageFromArray(new_arr)
    new_img.SetSpacing([2.5, 3.5, 4.5])
    writer = sitk.ImageFileWriter()
    # Use the study/series/frame of reference information given in the meta-data
    # dictionary and not the automatically generated information from the file IO
    writer.KeepOriginalImageUIDOn()

    modification_time = time.strftime("%H%M%S")
    modification_date = time.strftime("%Y%m%d")

    # Copy some of the tags and add the relevant tags indicating the change.
    # For the series instance UID (0020|000e), each of the components is a number,
    # cannot start with zero, and separated by a '.' We create a unique series ID
    # using the date and time. Tags of interest:
    direction = new_img.GetDirection()
    series_tag_values = [
        ("0008|0031", modification_time),  # Series Time
        ("0008|0021", modification_date),  # Series Date
        ("0008|0008", "DERIVED\\SECONDARY"),  # Image Type
        (
            "0020|000e",
            "1.2.826.0.1.3680043.2.1125."
            + modification_date
            + ".1"
            + modification_time,
        ),  # Series Instance UID
        (
            "0020|0037",
            "\\".join(
                map(
                    str,
                    (
                        direction[0],
                        direction[3],
                        direction[6],
                        direction[1],
                        direction[4],
                        direction[7],
                    ),
                )
            ),
        ),  # Image Orientation
        # (Patient)
        ("0008|103e", "Created-SimpleITK"),  # Series Description
    ]

    # Write slices to output directory
    list(
        map(
            lambda i: writeSlices(
                writer, series_tag_values, new_img, output_dir_dcm, i
            ),
            range(new_img.GetDepth()),
        )
    )

    # Re-read the series
    # Read the original series. First obtain the series file names using the
    # image series reader.
    data_directory = str(output_dir_dcm)
    series_IDs = sitk.ImageSeriesReader.GetGDCMSeriesIDs(data_directory)
    if not series_IDs:
        print(
            'ERROR: given directory "'
            + data_directory
            + '" does not contain a DICOM series.'
        )
        sys.exit(1)
    series_file_names = sitk.ImageSeriesReader.GetGDCMSeriesFileNames(
        data_directory, series_IDs[0]
    )

    series_reader = sitk.ImageSeriesReader()
    series_reader.SetFileNames(series_file_names)

    series_reader.LoadPrivateTagsOn()
    # save outputs
    # filepath_out_dcm is the name of the zip file that will be created
    # output_dir_dcm is the folder to zip
    # output_dir is the folder to save the zipped file after zipping it

    shutil.make_archive(filepath_out_dcm, "zip", output_dir_dcm)
    shutil.rmtree(output_dir_dcm)
