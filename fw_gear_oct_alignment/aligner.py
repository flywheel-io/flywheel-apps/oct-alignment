"""
Module for creating aligner interfaces (abstract objects) and their instantiations, used
for aligning files.
"""

import logging
import os
import time
from abc import ABC, abstractmethod
from pathlib import Path

import numpy as np
from pandas.util._decorators import Appender

import fw_gear_oct_alignment.oct_alignment as oct_alignment
import fw_gear_oct_alignment.reader_writer as rw

from .utils.docs import create_shared_doc_vars

log = logging.getLogger(__name__)

_shared_docs, _shared_doc_kwargs = create_shared_doc_vars()
_shared_doc_kwargs.update(
    {
        "file_type": """file_type: str
        An abstract class parameter implemented in the concrete objects, it is the type of file
         that contains the OCT slices to be aligned""",
        "file_ext": """file_ext: str
        An abstract class parameter implemented in the concrete objects, it is the file
        extension that is read in the factory() method to select the concrete subclass
        of OCTaligner.""",
        "path_file": """path_file: str
        The path the the file being read""",
        "output_dir": """output_dir: PathLike
        The output path of the output directory""",
        "oct_volumes": """oct_volumes: list
        Modified by a subclass' self.read_file() method, it contains the oct volumes (.npy or .dcm).""",
        "oct_volumes_aligned": """oct_volumes: list
        Modified by a subclass' self.align_files() method, it contains the oct volumes aligned (.npy).""",
        "output_csv": """output_csv: pandas dataframe
        Stores alignment metadata""",
        "config_dict": """config_dict: dict
        A dict containing the following keys:
        - smooth_level: int
            Kernel size of median blurring denoising algorithm. Possible values are: 1, 5, 9, 15, 19, 25
            or 29. Default is None.""",
        "OCTaligner": """The abstract base class that acts as an interface for oct-aligner objects.
        This object is mainly used to read a raw input file with self.factory() method, which selects which subclass
        to use for reading and saving OCT data, and then uses its template method self.align_file()
        to perform the alignment.""",
        "numpyOCTaligner": """An oct aligner for OCT slices stored in numpy arrays (.npy).""",
        "dicomOCTaligner": """An oct converter for OCT slices stored in Dicom files (.dcm).""",
        "zippedDicomOCTaligner": """An oct converter for OCT slices stored in zipped Dicom files (.dcm.zip).""",
    }
)
_shared_docs[
    "OCTaligner"
] = """
%(OCTaligner)s

Params
------
%(file_type)s
%(file_ext)s
%(path_file)s
%(output_dir)s
%(oct_volumes)s
%(oct_volumes_aligned)s
%(output_csv)s
"""


@Appender(_shared_docs["OCTaligner"] % _shared_doc_kwargs)
class OCTaligner(ABC):
    @classmethod
    def factory(cls, path_file, output_dir: os.PathLike, work_dir: os.PathLike):

        _, file_ext = os.path.splitext(path_file)

        file_exts = [sub.file_ext for sub in cls.__subclasses__()]

        result = None

        subclass_name = []
        for subclass in cls.__subclasses__():
            subclass_name.append(subclass.file_ext)
            if subclass.file_ext == file_ext:
                result = subclass(
                    path_file=path_file, output_dir=output_dir, work_dir=work_dir
                )
                break

        if not result:
            raise ValueError(
                f"Could not locate {cls.__name__} with file extension "
                f"Subclass: {subclass_name}"
                f"'{file_ext}'. Available file extensions: \n"
                f" {file_exts}"
            )

        return result

    def __init__(self, path_file, output_dir: os.PathLike, work_dir: os.PathLike):
        self.path_file = path_file
        self.output_dir = output_dir
        self.work_dir = work_dir
        self.oct_volumes = []
        self.oct_volumes_aligned = []
        self.output_csv = []
        self.dicom = []

    _shared_docs[
        "align_files"
    ] = """
    A template method that runs the methods for aligning raw OCT slices.
    
    Params
    ------
    %(config_dict)s
    """

    @Appender(_shared_docs["align_files"] % _shared_doc_kwargs)
    def align_files(self, config_dict: dict):
        log.info("Starting alignment...")

        # Read the raw file
        log.info("Reading raw file stored in %s ..." % self.path_file)
        # loads data
        self.read_file()
        log.info("\tFinished reading oct volume.")

        # Align the raw file
        log.info("Aligning OCT volumes...")
        self.align_files_starter(config_dict)
        log.info("\tFinished aligning oct volume.")

        # Save outputs
        log.info("Saving output types and headers (if applicable)...")
        self.save_output()

        # generate paths
        filepath_out_video = Path(self.output_dir).joinpath(
            Path(self.path_file).stem + "_aligned.mp4"
        )
        filepath_out_csv = Path(self.output_dir).joinpath(
            Path(self.path_file).stem + "_alignment_metadata.csv"
        )

        imArr_aligned = np.uint8(self.oct_volumes_aligned)
        log.info("Saving video as .mp4...")
        # generate video of aligned slices:
        oct_alignment.generate_video(imArr_aligned, filepath_out_video)

        log.info("Saving metadata csv...")

        self.output_csv.to_csv(filepath_out_csv)
        log.info("All saved!")

        log.info("\tFinished.")

        pass

    @property
    @abstractmethod
    def file_type(self):
        """
        The file type of the raw input file to convert.
        """
        pass

    @property
    @abstractmethod
    def file_ext(self):
        """
        The extension of the raw input file to convert.
        """

    @abstractmethod
    def read_file(self) -> dict:
        """
        Loads OCT volume in ".npy" format
        """

        pass

    @abstractmethod
    def align_files_starter(self, config_dict: dict):
        """
        Aligns OCT volume (numpy array) and creates csv with alignment metadata.
        """

        pass

    @abstractmethod
    def save_output(self):
        """
        Saves aligned images as .npy and as a video (.mp4); saves also a csv file with metadata.
        """

        pass


@Appender(
    _shared_docs["OCTaligner"]
    % {
        **_shared_doc_kwargs,
        **{"OCTaligner": _shared_doc_kwargs["numpyOCTaligner"]},
    }
)
class numpyOCTaligner(OCTaligner):

    file_type = "numpy"
    file_ext = ".npy"

    def read_file(self):
        """
        Loads OCT volume in ".npy" format
        """

        self.oct_volumes = rw.read_oct_numpy(self.path_file)

    def align_files_starter(self, config_dict: dict):
        """
        Aligns OCT volume (numpy array) and creates csv with alignment metadata.
        Args:
            config_dict: dictionary with configuration options.
        """
        smooth_level = config_dict["smooth_level"]
        centering_criteria_percentage = config_dict["centering_criteria_percentage"]
        xOffset_limit = config_dict["xOffset_limit"]
        self.oct_volumes_aligned, self.output_csv = oct_alignment.batch_aligning(
            self.oct_volumes, smooth_level, centering_criteria_percentage, xOffset_limit
        )

    def save_output(self):
        """
        Saves aligned images as .npy.

        """
        fpath, fext = os.path.splitext(self.path_file)
        _, fname = os.path.split(fpath)
        log.info("\tSaving aligned oct volumes")

        log.debug("\tfilepath_out: %s", self.output_dir)

        filepath_out_npy = os.path.join(self.output_dir, fname + "_aligned.npy")
        log.info("Saving aligned images as numpy array...")
        np.save(filepath_out_npy, self.oct_volumes_aligned)

        log.info("Finished saving '%s'", self.output_dir)


@Appender(
    _shared_docs["OCTaligner"]
    % {
        **_shared_doc_kwargs,
        **{"OCTaligner": _shared_doc_kwargs["dicomOCTaligner"]},
    }
)
class dicomOCTaligner(OCTaligner):

    file_type = "dicom"
    file_ext = ".dcm"

    def read_file(self):
        """
        Loads OCT volume in ".dcm" format
        """

        self.dicom, self.oct_volumes = rw.read_oct_dicom(self.path_file)

    def align_files_starter(self, config_dict: dict):
        """
        Aligns OCT volume (numpy array) and creates csv with alignment metadata.
        Args:
            config_dict: dictionary with configuration options.
        """
        smooth_level = config_dict["smooth_level"]
        centering_criteria_percentage = config_dict["centering_criteria_percentage"]
        xOffset_limit = config_dict["xOffset_limit"]
        rotate_first_slice = config_dict["rotate_first_slice"]

        self.oct_volumes_aligned, self.output_csv = oct_alignment.batch_aligning(
            self.oct_volumes,
            smooth_level,
            centering_criteria_percentage,
            xOffset_limit,
            rotate_first_slice,
        )

    def save_output(self):
        """
        Saves aligned images as .dcm

        """
        fpath, fext = os.path.splitext(self.path_file)
        _, fname = os.path.split(fpath)

        log.info("\tSaving aligned oct volumes")

        log.debug("\tfilepath_out: %s", self.output_dir)

        filepath_out_dicom = os.path.join(self.output_dir, fname + "_aligned.dcm")
        log.info("Saving aligned images as dicom file...")

        # create new unique StudyInstanceUID and SeriesInstanceUID so that there are no conflicts in Flywheel site
        # with original dicom.
        modification_time = time.strftime("%H%M%S")
        modification_date = time.strftime("%Y%m%d")
        seriesInstanceUID = (
            "1.2.826.0.1.3680043.2.1125." + modification_date + ".1" + modification_time
        )
        time.sleep(2)
        modification_time = time.strftime("%H%M%S")
        modification_date = time.strftime("%Y%m%d")
        studyInstanceUID = (
            "1.2.826.0.1.3680043.2.1125." + modification_date + ".1" + modification_time
        )
        self.dicom.seriesInstanceUID = seriesInstanceUID
        self.dicom.studyInstanceUID = studyInstanceUID

        # dicom stores pixel_array in bytes
        self.dicom.PixelData = (
            self.oct_volumes_aligned.astype("uint8").flatten().tobytes()
        )
        self.dicom.save(filepath_out_dicom)

        log.info("Finished saving '%s'", filepath_out_dicom)


@Appender(
    _shared_docs["OCTaligner"]
    % {
        **_shared_doc_kwargs,
        **{"OCTaligner": _shared_doc_kwargs["zippedDicomOCTaligner"]},
    }
)
class zippedDicomOCTaligner(OCTaligner):

    file_type = "zip"
    file_ext = ".zip"

    def read_file(self):
        """
        Loads OCT volume in ".dcm.zip" format
        """

        self.oct_volumes = rw.read_oct_dicom_zip(self.path_file)

    def align_files_starter(self, config_dict: dict):
        """
        Aligns OCT volume (numpy array) and creates csv with alignment metadata.
        Args:
            config_dict: dictionary with configuration options.
        """
        smooth_level = config_dict["smooth_level"]
        centering_criteria_percentage = config_dict["centering_criteria_percentage"]
        xOffset_limit = config_dict["xOffset_limit"]
        self.oct_volumes_aligned, self.output_csv = oct_alignment.batch_aligning(
            self.oct_volumes, smooth_level, centering_criteria_percentage, xOffset_limit
        )

    def save_output(self):
        """
        Saves aligned images as .dcm.zip

        """
        fpath, fext = os.path.splitext(self.path_file)
        _, fname = os.path.split(fpath)
        fname = fname.split(".")[0]
        log.info("\tSaving aligned oct volumes")

        log.debug("\tfilepath_out: %s", self.output_dir)
        filepath_out_dcm = os.path.join(self.output_dir, fname + "_aligned.dcm")

        log.info("Saving aligned images as DICOM zipped file...")
        rw.process_zipped_dicom(
            filepath_out_dcm, self.oct_volumes_aligned, self.work_dir
        )

        log.info("Finished saving '%s'", filepath_out_dcm)
