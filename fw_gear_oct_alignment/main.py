"""Main module."""
import logging
import sys

from .aligner import OCTaligner

log = logging.getLogger(__name__)


def run(ophtha_aligner: OCTaligner, config_dict):
    """
    Uses normalize crosscorrelation to align retinal images from OCT slices.

    Args:
    ---------
    ophtha_aligner: OCTaligner
        A concrete instance of OCTaligner.
    config_dict: dict
        A dict containing the following keys:
        - smooth_level: int
            Optional - Kernel size of median blurring denoising algorithm.

    """
    log.info("Starting OCT-alignment gear...")
    try:
        # Call the template method for aligning OCT slices
        ophtha_aligner.align_files(config_dict=config_dict)
    except Exception:
        log.exception("OCT alignment failed")
        log.info("Exiting...")
        sys.exit(1)
    else:
        log.info("Done with OCT-alignment gear.")
        return 0
