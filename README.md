# OCT-alignment
This gear aligns raw OCT slice data to correct for motion artifacts during recording.

The gear allows OCT slice data in both numpy array or zipped DICOM format. The aligned slices will be output in the same
format as the input. A video (.mp4) is created to quickly view across slices. The gear also creates a csv file with metadata
of the alignment.

The alignment algorithm uses the python implementation of Matlab's normcross2: [normxcorr2-python](https://github.com/Sabrewarrior/normxcorr2-python).

## Usage

### Inputs

* __raw_input__: Raw OCT slices (.npy or .dcm.zip)

### Configuration
* __debug__ (boolean, default False): Include debug statements in output.
* __smooth_level__ (integer, default 0): Kernel size for median blurring denoising algorithm of OCT images. Default is no denoising.
* __centering_criteria_percentage__ (integer, default 5): The first slice will be centered until the retina is +- this centering criteria * image height.
* __xOffset_limit__ (integer, default 15): X offset shift limitation on alignment to avoid carryover misalignment.

## Contributing

For more information about how to get started contributing to that gear, 
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
