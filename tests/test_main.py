"""Module to test main.py"""
import os
import shutil
from pathlib import Path

from fw_gear_oct_alignment.aligner import OCTaligner
from fw_gear_oct_alignment.main import run

"""
Dataset used for tests publicly available:
Lakshminarayanan, Vasudevan; Roy, Priyanka; Gholami, Peyman, 2018, "Normal Retinal OCT images", https://doi.org/10.5683/SP/WLW4ZT, Scholars Portal Dataverse, V1
"""


def test_numpy(tmpdir):
    config_dict = {
        "smooth_level": 0,
        "centering_criteria_percentage": 5,
        "xOffset_limit": 15,
    }
    path_raw_input = os.path.abspath("tests/assets/oct_volume_test.npy")

    output_dir = Path(tmpdir.mkdir("output"))
    work_dir = Path(tmpdir.mkdir("work_dir"))

    print("Saving files in %s", output_dir)

    ophtha_aligner = OCTaligner.factory(
        path_file=path_raw_input, output_dir=output_dir, work_dir=work_dir
    )

    exit_code = run(ophtha_aligner=ophtha_aligner, config_dict=config_dict)
    assert exit_code == 0


def test_dicom(tmpdir):

    config_dict = {
        "smooth_level": 0,
        "centering_criteria_percentage": 5,
        "xOffset_limit": 15,
        "rotate_first_slice": False,
    }
    path_raw_input = os.path.abspath("tests/assets/oct_volume_test.dcm")

    output_dir = Path(tmpdir.mkdir("output"))
    work_dir = Path(tmpdir.mkdir("work_dir"))

    print("Saving files in %s", output_dir)

    ophtha_aligner = OCTaligner.factory(
        path_file=path_raw_input, output_dir=output_dir, work_dir=work_dir
    )

    exit_code = run(ophtha_aligner=ophtha_aligner, config_dict=config_dict)

    assert exit_code == 0


def test_zippedDicom(tmpdir):

    config_dict = {
        "smooth_level": 0,
        "centering_criteria_percentage": 5,
        "xOffset_limit": 15,
    }
    path_raw_input = os.path.abspath("tests/assets/oct_volume_test.dcm.zip")

    output_dir = Path(tmpdir.mkdir("output"))
    work_dir = Path(tmpdir.mkdir("work_dir"))

    print("Saving files in %s", output_dir)

    ophtha_aligner = OCTaligner.factory(
        path_file=path_raw_input, output_dir=output_dir, work_dir=work_dir
    )

    exit_code = run(ophtha_aligner=ophtha_aligner, config_dict=config_dict)

    assert exit_code == 0
