from pathlib import Path

from fw_gear_oct_alignment.aligner import *

"""
Dataset used for tests publicly available:
Lakshminarayanan, Vasudevan; Roy, Priyanka; Gholami, Peyman, 2018, "Normal Retinal OCT images", https://doi.org/10.5683/SP/WLW4ZT, Scholars Portal Dataverse, V1
"""


def test_factory_read_works_numpy(tmpdir):
    """
    Check instantiation of OCTaligner with numpy input
    """
    output_dir = Path(tmpdir.mkdir("output"))
    work_dir = Path(tmpdir.mkdir("work_dir"))

    path_file = os.path.abspath("tests/assets/oct_volume_test.npy")

    ophtha_aligner = OCTaligner.factory(
        path_file=path_file, output_dir=output_dir, work_dir=work_dir
    )

    _, file_ext = os.path.splitext(path_file)

    assert ophtha_aligner.file_ext == file_ext
    assert ophtha_aligner.file_type == "numpy"


def test_factory_read_works_dicom(tmpdir):
    """
    Check instantiation of OCTaligner with dicom zip as input
    """
    output_dir = Path(tmpdir.mkdir("output"))
    work_dir = Path(tmpdir.mkdir("work_dir"))

    path_file = os.path.abspath("tests/assets/oct_volume_test.dcm")

    ophtha_aligner = OCTaligner.factory(
        path_file=path_file, output_dir=output_dir, work_dir=work_dir
    )

    _, file_ext = os.path.splitext(path_file)

    assert ophtha_aligner.file_ext == file_ext
    assert ophtha_aligner.file_type == "dicom"


def test_factory_read_works_zippeddicom(tmpdir):
    """
    Check instantiation of OCTaligner with dicom zip as input
    """
    output_dir = Path(tmpdir.mkdir("output"))
    work_dir = Path(tmpdir.mkdir("work_dir"))

    path_file = os.path.abspath("tests/assets/oct_volume_test.dcm.zip")

    ophtha_aligner = OCTaligner.factory(
        path_file=path_file, output_dir=output_dir, work_dir=work_dir
    )

    _, file_ext = os.path.splitext(path_file)

    assert ophtha_aligner.file_ext == file_ext
    assert ophtha_aligner.file_type == "zip"
